#!/usr/bin/env python3

import argparse
import yaml
import logging
import time

from twitchio.ext import commands
from obswebsocket import obsws, requests
import asyncio


class Action:
    def __init__(self, args):
        self.args = args

    async def run(self, context, obs, message):
        raise NotImplementedError


class ChangeSceneAction(Action):
    def __init__(self, args):
        super().__init__(args)

    async def run(self, context, obs, message):
        currentScene = obs.call(requests.GetCurrentScene())
        logging.debug("Current scene is %s", currentScene.getName())
        if not currentScene.getName() in self.args['from']:
            logging.info("Current scene %s is not in %s", currentScene.getName(), self.args['from'])
            return False

        logging.debug("Switching to scene %s", self.args['to'])
        obs.call(requests.SetCurrentScene(self.args['to']))
        return True


class SayHelloAction(Action):
    def __init__(self, args):
        super().__init__(args)

    async def run(self, context, obs, message):
        await message.channel.send(f'Hello {message.author.name}!')
        return True


class PingAction(Action):
    def __init__(self, args):
        super().__init__(args)

    async def run(self, context, obs, message):
        await message.channel.send(f'pong')
        return True


class ToggleSourceAction(Action):
    def __init__(self, args):
        super().__init__(args)

    async def run(self, context, obs, message):
        source_name = self.args['source']
        scene_name = self.args.get('scene')
        properties = obs.call(requests.GetSceneItemProperties(scene_name=scene_name, item=source_name))
        try:
            source = obs.call(requests.SetSceneItemProperties(scene_name=scene_name, item=source_name, visible=(not properties.getVisible())))
        except Exception as e:
            logging.error("Could not toggle visibility of source %s:%s: %s", scene_name, source_name, e)
            return False
        return True

KNOWN_ACTIONS = {
    "change_scene": ChangeSceneAction,
    "say_hello": SayHelloAction,
    "ping": PingAction,
    "toggle_source": ToggleSourceAction,
}


# Authorization exceptions

class NotInAllowList(Exception):
    pass

class NotModerator(Exception):
    pass

class Timeout(Exception):
    pass


class Authorizer():
    def __init__(self, timeout = None, users = [], is_moderator = False):
        self.timeout = timeout
        # Initialize last time (for timeout)
        self.last = 0

        self.users = users
        self.is_moderator = is_moderator

    def authorize(self, message):
        # Check users and moderator first
        if self.users and not message.author.name in self.users:
            logging.debug("%s is not in authorized users list", message.author.name)
            raise NotInAllowList("user is not in authorized users list")

        if self.is_moderator and not message.author.is_mod:
            logging.debug("%s is not moderator", message.auhor.name)
            raise NotModerator("user is not moderator")

        # End with timeout
        now = time.time()
        if self.timeout and now - self.last < self.timeout:
            logging.debug("Timeout (%d seconds remaining)", self.timeout - (now - self.last))
            raise Timeout("timeout (%d seconds remaining)" % (self.timeout - (now - self.last)))

        return True

    def done(self, timestamp):
        logging.debug("Set last to %s", timestamp)
        self.last = timestamp


class Group:
    def __init__(self, name, opts):
        self.name = name
        self.authorizer = Authorizer(**opts.get('authorization', {}))

    def authorize(self, message):
        return self.authorizer.authorize(message)

    def done(self, timestamp):
        self.authorizer.done(timestamp)


class Command:
    def __init__(self, name, opts):
        self.name = name
        self.actions = []
        for action in opts['actions']:
            assert(len(action) == 1)
            for (action_name, action_opts) in action.items():
                self.actions.append(KNOWN_ACTIONS[action_name](action_opts))

        self.groups = opts.get('groups', [])
        self.authorizer = Authorizer(**opts.get('authorization', {}))

    def authorize(self, context, message):
        # Check command first
        try:
            self.authorizer.authorize(message)

            # Check groups next
            for group in self.groups:
                context.groups[group].authorize(message)
        except:
            raise

    def done(self, context):
        timestamp = time.time()
        self.authorizer.done(timestamp)
        for group in self.groups:
            context.groups[group].done(timestamp)

    async def run(self, context, obs, message):
        try:
            self.authorize(context, message)
        except Exception as e:
            logging.info("Command %s unauthorized for %s: %s", self.name, message.author.name, e)
            return False

        # Execute actions, and stop if one action fails
        for action in self.actions:
            if not await action.run(context, obs, message):
                return

        # Mark command as done
        self.done(context)


class Context:
    def __init__(self, config):
        self._groups = {}
        for (name, opts) in config['groups'].items():
            self._groups[name] = Group(name, opts)

        self._commands = {}
        for (command, opts) in config['commands'].items():
            self._commands[command] = Command(command, opts)

    @property
    def commands(self):
        return self._commands

    @property
    def groups(self):
        return self._groups


class Bot(commands.Bot):

    def __init__(self, config, context, obs):
        super().__init__(
            irc_token = config['twitch']['tmi_token'],
            client_id = config['twitch']['client_id'],
            nick = config['twitch']['bot_nick'],
            prefix = config['twitch']['bot_prefix'],
            initial_channels = [config['twitch']['channel']],
        )
        self.prefix = config['twitch']['bot_prefix']
        self.nick = config['twitch']['bot_nick']
        self.context = context
        self.obs = obs
        self.lock = asyncio.Lock()

    async def event_ready(self):
        logging.info(f'Ready | {self.nick}')
        await self._ws.send_privmsg(
            self.nick,
            f"/me Commandes: %s" % ', '.join(
                ['!%s' % cmd for cmd in self.context.commands.keys()]
            )
        )

    async def event_message(self, message):
        # Only one message can be processed at a time
        async with self.lock:
            logging.debug("Received a message: %s", message.content)

            content = message.content.lower()
            if content[0] != self.prefix:
                return

            command = content[1:]
            if command in self.context.commands:
                logging.debug("Command %s recognized", command)
                await self.context.commands[command].run(self.context, self.obs, message)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--config", help="Config file", required=True)
    parser.add_argument("-d", "--debug", help="Debug mode", action="store_true")
    args = parser.parse_args()

    # Logging configuration
    loglevel = logging.DEBUG if args.debug else logging.INFO
    logging.basicConfig(level=loglevel)

    # Load config
    config = yaml.safe_load(open(args.config, 'r'))
    context = Context(config)

    # Set obs
    obs = obsws(
        config['obs']['host'],
        config['obs']['port'],
        config['obs']['password']
    )
    obs.connect()

    # Setup twitch bot
    bot = Bot(config, context, obs)
    bot.run()

    obs.disconnect()

if __name__ == "__main__":
    main()
